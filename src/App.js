import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import userActions from './store/actions/user.action';
import MyForm from './components/MyForm';

const App = () => {
	const state = useSelector((s) => s.userReducer);

	const dispatch = useDispatch();
	function handleSubmit(userForm) {
		dispatch(userActions.createUser(userForm));
	}

	return (
		<div>
			<div>Add new User</div>
			<MyForm onSubmit={handleSubmit} />
			<hr />
			<h2>List of users</h2>
			<ul>{state.map((user, index) => <li key={index}>{user.username}</li>)}</ul>
		</div>
	);
};

export default App;
