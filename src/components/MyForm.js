import React from 'react';
import { Field, reduxForm } from 'redux-form';
import renderField from './form/renderField';

const validate = (values) => {
	const errors = {};
	if (!values.username) {
		errors.username = 'Required';
	} else if (values.username.length > 15) {
		errors.username = 'Must be 15 characters or less';
	}
	if (!values.email) {
		errors.email = 'Required';
	} else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
		errors.email = 'Invalid email address';
	}
	if (!values.age) {
		errors.age = 'Required';
	} else if (isNaN(Number(values.age))) {
		errors.age = 'Must be a number';
	} else if (Number(values.age) < 18) {
		errors.age = 'Sorry, you must be at least 18 years old';
	}
	if (!values.password) {
		errors.password = 'Required';
	}
	return errors;
};

const warn = (values) => {
	const warnings = {};
	if (values.age < 19) {
		warnings.age = 'Hmm, you seem a bit young...';
	}
	return warnings;
};

let MyForm = (props) => {
	const { handleSubmit, pristine, reset, submitting } = props;
	return (
		<form onSubmit={handleSubmit}>
			<div>
				<Field name="username" type="text" component={renderField} label="Username:" />
			</div>
			<div>
				<Field name="email" type="email" component={renderField} label="Email:" />
			</div>
			<div>
				<Field name="age" type="number" component={renderField} label="Age:" />
			</div>
			<div>
				<Field name="password" type="password" component={renderField} label="Password:" />
			</div>
			<div>
				<button type="submit" disabled={submitting}>
					Submit
				</button>
				<button type="button" disabled={pristine || submitting} onClick={reset}>
					Clear Values
				</button>
			</div>
		</form>
	);
};

export default reduxForm({
	form: 'user', // a unique identifier for this form
	validate, // <--- validation function given to redux-form
	warn // <--- warning function given to redux-form
})(MyForm);
