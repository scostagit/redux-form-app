import React from 'react';
import { Field, reduxForm } from 'redux-form';

let UserForm = (props) => {
	const { handleSubmit } = props;

	return (
		<form onSubmit={handleSubmit}>
			<div>
				<label htmlFor="username">username:</label>
				<Field name="username" component="input" type="text" />
			</div>
			<div>
				<label htmlFor="email">email:</label>
				<Field name="email" component="input" type="email" />
			</div>
			<div>
				<label htmlFor="password">password:</label>
				<Field name="password" component="input" type="password" />
			</div>

			<div>
				<button type="submit">Submit</button>
			</div>
		</form>
	);
};

UserForm = reduxForm({
	form: 'user'
})(UserForm);

export default UserForm;
