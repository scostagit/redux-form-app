import userConstants from '../constants/user.constant';

export default function userReducer(state = [], action) {
	switch (action.type) {
		case userConstants.USER_FETCH_BEGIN:
			return Object.assign({}, state, { loading: true });
		case userConstants.USER_FETCH_SUCCESS:
			return [ ...state, action.payload ];
		case userConstants.USER_FETCH_ERROR:
			return [ ...state, { error: action.payload.error } ];
		default:
			return state;
	}
}
