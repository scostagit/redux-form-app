import userConstants from '../constants/user.constant';

function createUser(user) {
	return (dispatch) => {
		dispatch(fetchLogin);
		dispatch(fetchSuccess(user));
	};

	function fetchLogin() {
		return { type: userConstants.USER_FETCH_BEGIN };
	}
	function fetchSuccess(user) {
		return { type: userConstants.USER_FETCH_SUCCESS, payload: user };
	}
	// function fetchError(err) {
	// 	return { type: userConstants.USER_FETCH_ERROR, payload: err };
	// }
}

export default {
	createUser
};
