import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import userReducer from './reducers/user.reducer';
import { createLogger } from 'redux-logger';
import { reducer as formReducer } from 'redux-form';

const loggerMiddleware = createLogger();

const rootReducer = combineReducers({ userReducer, form: formReducer });

const store = createStore(rootReducer, applyMiddleware(thunkMiddleware, loggerMiddleware));

export default store;
